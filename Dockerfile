FROM python:3

WORKDIR /usr/src/app

RUN pip install --upgrade setuptools pyopenssl firebase-admin

COPY . .

CMD [ "python", "./fcm_server.py" ]

